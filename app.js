var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var response = require('./util/appResponse');
var serviceMessage = require('./service/serviceMessage');
var userService = require('./service/user');

require('custom-env').env('development');


let mongoose = require('mongoose');

mongoose.connect('mongodb://' + process.env.MONGO_DB_HOST + ":" + process.env.MONGO_DB_PORT + "/" + process.env.MONGO_DB_NAME);

mongoose.connection.on('connected', function () {
  console.log('Mongoose default connection opened- ' + process.env.MONGO_DB_NAME);
});

mongoose.connection.on('error', function (err) {
  console.log('Mongoose default connection error: ' + err);
});

mongoose.connection.on('disconnected', function () {
  console.log('Mongoose default connection disconnected');
});

// disconnect mongodb connection upon app shutdown
process.on('SIGINT', function () {
  mongoose.connection.close(function () {
    console.log('Mongoose default connection disconnected through app termination');
    process.exit(0);
  });
});


var usersRouter = require('./routes/users');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

var exceptionAuth = ['/api/user/login', '/api/user/registration'];
app.use(async function(req, res, next) {
  if (req.headers.authtoken && !exceptionAuth.includes(req.path)) {
    try {
      decodedData = await userService.decodeJWTToken(req.headers.authtoken);
      req.headers.userinfo = decodedData;
      next()
    }
    catch (err) {
      response.sendResponse(res, "UNAUTHORIZED", "FAILURE", serviceMessage.getMessage("UNAUTHORIZED"), "");
    }
  } else if (exceptionAuth.includes(req.path)) {
    next()
  }
  else {
    response.sendResponse(res, "UNAUTHORIZED", "FAILURE", serviceMessage.getMessage("UNAUTHORIZED"), "");
  }
})

app.use('/api/user', usersRouter);


// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

app.listen(process.env.SERVER_PORT);

module.exports = app;
