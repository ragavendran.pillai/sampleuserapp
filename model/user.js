let mongoose = require('mongoose')

let userSchema = new mongoose.Schema({
    name:String,
    email:String,
    password:String,
    salt:String,
    image:String
})

module.exports = mongoose.model('User', userSchema)