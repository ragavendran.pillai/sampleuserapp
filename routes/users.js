var express = require('express');
var router = express.Router();
var userService = require('../service/user');
var response = require('../util/appResponse');
var serviceMessage = require('../service/serviceMessage');

router.post('/registration', async (req, res, next) =>{
  try {
    var data = req.body;
    userDetail = await userService.registerUser(data);
    response.sendResponse(res,"SUCCESS",userDetail.STATUS,userDetail.MESSAGE,userDetail.DATA);
  }
  catch (ex) {
    response.sendResponse(res,"ERROR","FAILURE",serviceMessage.getMessage("DEFAULT_EXCEPTION_MESSAGE"),"");
  }
});

router.post('/login', async (req, res, next) => {
  try {
    var email = req.body.email;
    var password = req.body.password; 
    userDetail = await userService.authenticateUser(email,password);
    response.sendResponse(res,"SUCCESS",userDetail.STATUS,userDetail.MESSAGE,userDetail.DATA);
  }
  catch (ex) {
    response.sendResponse(res,"ERROR","FAILURE",serviceMessage.getMessage("DEFAULT_EXCEPTION_MESSAGE"),"");
  }
});

router.get('/all', async (req, res, next) => {
  try {
    userDetail = await userService.userDetails();
    response.sendResponse(res,"SUCCESS",userDetail.STATUS,userDetail.MESSAGE,userDetail.DATA);
  }
  catch (ex) {
    response.sendResponse(res,"ERROR","FAILURE",serviceMessage.getMessage("DEFAULT_EXCEPTION_MESSAGE"),"");
  }
});

router.get('/refreshtoken/get', async (req, res, next) => {
  try {
    userDetail = await userService.getRefreshToken(req.headers.userinfo.userID);
    response.sendResponse(res,"SUCCESS",userDetail.STATUS,userDetail.MESSAGE,userDetail.DATA);
  }
  catch (ex) {
    response.sendResponse(res,"ERROR","FAILURE",serviceMessage.getMessage("DEFAULT_EXCEPTION_MESSAGE"),"");
  }
});

module.exports = router;
