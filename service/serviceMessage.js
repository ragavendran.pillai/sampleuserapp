const messages = {
    "DEFAULT_EXCEPTION_MESSAGE":"Unexpected error occurred,Please contact administrator",
    "INPUT_MISSING":"Missing Input Data",
    "REGISTRATION_FAILED":"User Registration Failed",
    "REGISTRATION_SUCCESS":"User Registered Successfully",
    "USER_ALREADY_EXISTS":"Email ID already registered",
    "USER_DETAIL_FETCH_SUCCESS":"User detail fetched successfully",
    "USER_IMAGE_SAVE_SUCCESS":"User image updated successfully",
    "USER_REGISTRATION_SUBJECT":"User account creation",
    "ACKNOWLEDGEMENT_EMAIL":`Dear {name} your account has been successfully created.Now you can login with your email - {email} and password you had created during registration.Thank you.`,
    "EMAIL_SUCCESS":"Email sended successfully",
    "EMAIL_FAILURE":"Email sending failed",
    "REFRESH_TOKEN_SUCCESS":"Refrsh token created successfully",
    "JWT_TOKEN_SUCCESS":"JWT token created successfully",
    "UNAUTHORIZED":"Token Expired!",
    "REFRESH_TOKEN":"Refresh Token obtained"
}

function getMessage(code){
    return (messages[code]) ? messages[code] : "";
}

module.exports = {
    getMessage
}