const response = require('../util/appResponse');
const serviceMessage = require('./serviceMessage');
const passwordUtil = require('../util/password');
const UserModel = require('../model/user');
var RefreshTokenModel = require('../model/refreshToken');
const momentobj = require('moment');
const imageUTIL = require('../util/imageutil');
const path = require("path");
const fs = require('fs');
var nodemailer = require('nodemailer');
const jwt  = require('jsonwebtoken');

async function registerUser(payLoadData) {
    try {
        if (payLoadData.name && payLoadData.email && payLoadData.password) {
            var salt = passwordUtil.genRandomString(16);
            payLoadData.salt = salt;
            payLoadData.password = passwordUtil.hashPassword(payLoadData.password, salt);
            var imageData = "";

            if (payLoadData.image) {
                imageData = payLoadData.image;
                delete payLoadData.image;
            }

            userDetailVal = await getUserByEmail(payLoadData.email, false);

            if (userDetailVal.STATUS === "FAILURE") {
                let user = new UserModel(payLoadData);
                userDetail = await user.save();

                if (imageData != "") {
                    await storeandUpdateImage(payLoadData.email, imageData);
                }

                await sendMail(payLoadData);

                return response.internalMethodResponse("SUCCESS", serviceMessage.getMessage("REGISTRATION_SUCCESS"), "");
            }
            else {
                return response.internalMethodResponse("FAILURE", serviceMessage.getMessage("USER_ALREADY_EXISTS"), "");
            }
        }
        else {
            return response.internalMethodResponse("FAILURE", serviceMessage.getMessage("INPUT_MISSING"), "");
        }
    }
    catch (ex) {
        throw new Error(ex);
    }
}

async function getUserByEmail(email, includeCredential) {
    try {
        var userinfo = await UserModel.findOne({
            email: email
        }).lean().exec();

        if (userinfo) {
            if (!includeCredential) {
                delete userinfo['password'];
                delete userinfo['salt'];
            }
            return response.internalMethodResponse("SUCCESS", serviceMessage.getMessage("USER_DETAIL_FETCH_SUCCESS"), userinfo);
        }
        else {
            return response.internalMethodResponse("FAILURE", serviceMessage.getMessage("USER_DETAIL_NOT_FOUND"), "");
        }
    }
    catch (ex) {
        throw new Error(ex);
    }
}

async function storeandUpdateImage(email, imageData) {
    try {
        const ext = imageData.substring(imageData.indexOf("/") + 1, imageData.indexOf(";base64"));
        const fileType = imageData.substring("data:".length, imageData.indexOf("/"));
        const regex = new RegExp(`^data:${fileType}\/${ext};base64,`, 'gi');
        const base64Data = imageData.replace(regex, "");
        const rand = Math.ceil(Math.random() * 1000);
        const filename = `Pic_${Date.now()}_${rand}.${ext}`;
        const imgPath = path.join(process.env.IMG_BASE_PATH, filename);
        const filePath = path.join(__dirname, imgPath);

        if (!fs.existsSync(path.join(__dirname, process.env.IMG_BASE_PATH))) {
            fs.mkdirSync(path.join(__dirname, process.env.IMG_BASE_PATH));
        }

        fs.writeFileSync(filePath, base64Data, 'base64');
        await UserModel.findOneAndUpdate({ email: email }, { "image": imgPath })
        return response.internalMethodResponse("SUCCESS", serviceMessage.getMessage("USER_IMAGE_SAVE_SUCCESS"), "");
    }
    catch (ex) {
        throw new Error(ex);
    }
}


async function sendMail(payloadData) {
    try {
        const emailContent = serviceMessage.getMessage("ACKNOWLEDGEMENT_EMAIL").replace(
            /{(\w+)}/g,
            (placeholderWithDelimiters, placeholderWithoutDelimiters) =>
                payloadData[placeholderWithoutDelimiters] || placeholderWithDelimiters
        );

        var transporter = nodemailer.createTransport({
            service: process.env.EMAIL_HOST,
            auth: {
                user: process.env.EMAIL_AUTH_USER,
                pass: process.env.EMAIL_AUTH_PASSWORD
            }
        });

        var mailOptions = {
            from: process.env.EMAIL_FROM,
            to: payloadData.email,
            subject: serviceMessage.getMessage("USER_REGISTRATION_SUBJECT"),
            text: emailContent
        };

        let info = await transporter.sendMail(mailOptions);

        if (!info.messageId) {
            return response.internalMethodResponse("FAILURE", serviceMessage.getMessage("EMAIL_FAILURE"), "");
        }

        return response.internalMethodResponse("SUCCESS", serviceMessage.getMessage("USER_IMAGE_SAVE_SUCCESS"), "");
    }
    catch (ex) {
        return response.internalMethodResponse("FAILURE", serviceMessage.getMessage("EMAIL_FAILURE"), "");
    }
}

async function authenticateUser(email, password) {
    try {
        if (email && password) {
            userDetailVal = await getUserByEmail(email, true);
            if (userDetailVal.STATUS === "SUCCESS") {
                userDetail = userDetailVal.DATA
                if (passwordUtil.validatePassword(password, userDetail.password, userDetail.salt)) {
                    delete userDetail.password;
                    delete userDetail.salt;
                    let refreshTokenObj = await createandUpdateRefreshToken(userDetail._id);
                    let jwtTokenObj = await generateJWTToken(userDetail);

                    var obj = {
                        userDetail,
                        "refreshToken": refreshTokenObj.DATA,
                        "authToken":jwtTokenObj.DATA
                    }
                    return response.internalMethodResponse("SUCCESS", serviceMessage.getMessage("USER_LOGIN_SUCCESS"), obj);
                }
                else {
                    return response.internalMethodResponse("FAILURE", serviceMessage.getMessage("PASSWORD_INCORRECT"), "")
                }
            }
            else {
                return userDetailVal;
            }
        }
        else {
            return response.internalMethodResponse("FAILURE", serviceMessage.getMessage("INPUT_MISSING"), {});
        }
    }
    catch (ex) {
        throw new Error(ex);
    }
}

async function createandUpdateRefreshToken(userID) {
    try {
        var refreshTokenConfig = process.env.JWT_REFRESH_TOKEN_EXPIRY_TIME.split("-");
        var refreshTokenInput = {
            user: userID,
            token: passwordUtil.genRandomString(40),
            expires: momentobj(new Date()).add(refreshTokenConfig[0], refreshTokenConfig[1]).toDate()
        }

        let refreshToken = new RefreshTokenModel(refreshTokenInput);
        let refreshTokenOutput = await refreshToken.save();
        return response.internalMethodResponse("SUCCESS", serviceMessage.getMessage("REFRESH_TOKEN_SUCCESS"), refreshTokenOutput.token);
    }
    catch (ex) {
        throw new Error(ex);
    }
}


async function generateJWTToken(userDetail) {
    try {
        var payload = {
            "emailID": userDetail.email,
            "userID": userDetail._id
        }

        var privateKEY = fs.readFileSync(path.resolve(__dirname, "../safe/private.key"), 'utf8');
    
        var i = process.env.JWT_ISSUER;          // Issuer 
        var s = userDetail.email;        // Subject 
        var a = process.env.JWT_AUDIENCE; // Audience

        // SIGNING OPTIONS
        var signOptions = {
            issuer: i,
            subject: s,
            audience: a,
            expiresIn: process.env.JWT_AUTH_TOKEN_EXPIRY_TIME.replace("-",""),
            algorithm: "RS256"
        };

        var token = jwt.sign(payload, privateKEY, signOptions);
        return response.internalMethodResponse("SUCCESS", serviceMessage.getMessage("JWT_TOKEN_SUCCESS"),token);
    }
    catch (ex) {
        throw ex;
    }
}

async function userDetails(){
    try{
        let userDetails = await UserModel.find({},{password:0,salt:0}).lean().exec();
        return response.internalMethodResponse("SUCCESS", serviceMessage.getMessage("USER_DETAIL_FETCH_SUCCESS"),userDetails);
    }
    catch(ex){
        throw ex;
    }
}

async function decodeJWTToken(token){
        try{
            var cert = fs.readFileSync(path.resolve(__dirname, "../safe/public.key"));
            var decoded = await jwt.verify(token, cert) ;
            return decoded;
        }
        catch(ex){
            throw ex
        }
}

async function getRefreshToken(userID){
    try{
        var refreshTokenObj = await RefreshTokenModel.findOne({user:userID}).sort({created:-1}).lean().exec();
        return response.internalMethodResponse("SUCCESS", serviceMessage.getMessage("REFRESH_TOKEN"),refreshTokenObj.token);
    }
    catch(ex){
        throw ex;
    }
}

module.exports = {
    registerUser,
    storeandUpdateImage,
    authenticateUser,
    userDetails,
    decodeJWTToken,
    getRefreshToken
}