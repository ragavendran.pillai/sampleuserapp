var statusCodes = require('./statusCodes');
function internalMethodResponse(status, message, data) {
    return {
        "STATUS": status,
        "MESSAGE": message,
        "DATA": data
    }
}

function sendResponse(res,code,status,message,data) {
    statusCode = statusCodes.getStatusCode(code);
    res.status(statusCode)
    res.send({
        status,
        message,
        data
    })
}

module.exports = {
    internalMethodResponse,
    sendResponse
}