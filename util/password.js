var crypto = require('crypto'); 

function hashPassword(password,salt){
    var hash = crypto.pbkdf2Sync(password, salt,1000, 64, `sha512`).toString(`hex`); 
    return hash;
}

function validatePassword(plainPassword,dbPassword,salt){
    var hash = crypto.pbkdf2Sync(plainPassword, salt,1000, 64, `sha512`).toString(`hex`); 
    if(hash === dbPassword){
        return true
    }
    return false;
}

function genRandomString(length){
    return crypto.randomBytes(length).toString('hex');
};

module.exports = {
    hashPassword,
    validatePassword,
    genRandomString
}