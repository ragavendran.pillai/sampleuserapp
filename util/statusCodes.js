const statusCode = {
    "SUCCESS":200,
    "ERROR":500,
    "UNAUTHORIZED":401
}

function getStatusCode(code){
    return (statusCode[code]) ? statusCode[code] : statusCode['ERROR'];
}

module.exports = {
    getStatusCode
}